package com.abedev.wahwbonus;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by home on 20.09.2016.
 */
public class CropImageView extends View {
    private Uri uri;
    private Bitmap bitmap = null;
    private Matrix matrix;
    private float scaleToOriginal;
    private int originalWidth;
    private int originalHeight;
    private int dX;
    private int dY;
    private int selectionStartX;
    private int selectionStartY;
    private int selectionEndX;
    private int selectionEndY;
    Rect selectionRect = null;
    Paint selectionPaint;

    public CropImageView(Context context) {
        super(context);
        setUp();
    }


    public CropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUp();
    }

    public CropImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUp();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CropImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setUp();
    }

    public void setUri(@Nullable Uri uri) {
        if (this.uri == null || !this.uri.equals(uri)) {
            this.uri = uri;
            clearBitmap();
            if (isAttachedToWindow())
                updateBitmap();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (uri != null)
            updateBitmap();

    }

    private void setUp() {
        selectionPaint = new Paint();
        selectionPaint.setStyle(Paint.Style.STROKE);
        selectionPaint.setStrokeWidth(3);
        selectionPaint.setColor(0xffff0000);
    }


    private void updateBitmap() {
        if (uri!=null) {
            InputStream is = null;
            try {
                is = getContext().getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                return;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, options);
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            originalWidth = options.outWidth;
            originalHeight = options.outHeight;
            int inSampleSize = 1;
            if (originalHeight > getMeasuredHeight() || originalWidth > getMeasuredWidth())
                inSampleSize = Math.max(inSampleSize,
                        Math.max(originalHeight / getMeasuredHeight(), originalWidth / getMeasuredWidth()));
            options.inSampleSize = inSampleSize;
            options.inJustDecodeBounds = false;
            try {
                is = getContext().getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                return;
            }
            bitmap = BitmapFactory.decodeStream(is, null, options);
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            float scale = Math.min(getMeasuredHeight() / (float) bitmap.getHeight(), getMeasuredWidth() / (float) bitmap.getWidth());
            scaleToOriginal = (float) originalHeight / bitmap.getHeight() / scale;
            dX = (int) (getMeasuredWidth() - bitmap.getWidth() * scale) / 2;
            dY = (int) (getMeasuredHeight() - bitmap.getHeight() * scale) / 2;
            matrix = new Matrix();
            matrix.preScale(scale, scale);
            matrix.postTranslate(dX, dY);
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (bitmap != null)
            canvas.drawBitmap(bitmap, matrix, null);
        if (selectionRect != null)
            canvas.drawRect(selectionRect, selectionPaint);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearBitmap();
    }

    private void clearBitmap() {
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        selectionRect = null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (bitmap != null)
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    selectionStartX = (int) event.getX();
                    selectionStartY = (int) event.getY();
                    selectionEndX = (int) event.getX();
                    selectionEndY = (int) event.getY();
                    selectionRect = new Rect();
                    updateSelectionRect();
                    break;

                case MotionEvent.ACTION_MOVE:
                    if (selectionRect != null) {
                        selectionEndX = (int) event.getX();
                        selectionEndY = (int) event.getY();

                        if (Math.abs(selectionStartX - selectionEndX) > 5 || Math.abs(selectionStartY - selectionEndY) > 5) {
                            updateSelectionRect();
                        }
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    if (selectionRect != null)
                        updateSelectionRect();
                    break;

                default:
                    break;
            }
        return true;
    }

    private void updateSelectionRect() {
        if (selectionRect != null) {
            selectionRect.set(Math.min(selectionStartX, selectionEndX), Math.min(selectionStartY, selectionEndY), Math.max(selectionStartX, selectionEndX), Math.max(selectionStartY, selectionEndY));
            invalidate();
        }
    }

    @Nullable
    public Rect getCropRect() {
        Rect result = null;
        if (bitmap!= null && selectionRect!=null) {
            int left = Math.max((int)((selectionRect.left-dX)*scaleToOriginal),0);
            int top = Math.max((int)((selectionRect.top-dY)*scaleToOriginal),0);
            int right = Math.min((int)((selectionRect.right-dX)*scaleToOriginal),originalWidth);
            int bottom = Math.min((int)((selectionRect.bottom-dY)*scaleToOriginal),originalHeight);
            result = new Rect(left,top,right,bottom);
        }
        return result;

    }
}
