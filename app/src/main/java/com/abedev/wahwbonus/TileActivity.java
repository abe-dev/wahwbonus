package com.abedev.wahwbonus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class TileActivity extends AppCompatActivity {

    private static final String EXTRA_URI = "uri";
    private static final String EXTRA_RECT = "rect";
    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tile);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Uri uri = getIntent().getParcelableExtra(EXTRA_URI);
        Rect rect = getIntent().getParcelableExtra(EXTRA_RECT);
        try {
            InputStream is = getContentResolver().openInputStream(uri);
            BitmapRegionDecoder rd = BitmapRegionDecoder.newInstance(is,false);
            bitmap = rd.decodeRegion(rect,null);

        } catch (IOException e) {
            Toast.makeText(this, "Oops!", Toast.LENGTH_SHORT).show();;
        }
        if (bitmap!=null) {
            MyDrawable drawable = new MyDrawable(bitmap);
            ImageView imageView = (ImageView) findViewById(R.id.img);
            imageView.setImageDrawable(drawable);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bitmap!=null){
            bitmap.recycle();
            bitmap = null;
        }
    }

    public static Intent getIntent(Context context, Uri uri, Rect rect) {
        Intent intent = new Intent(context, TileActivity.class);
        intent.putExtra(EXTRA_URI,uri);
        intent.putExtra(EXTRA_RECT,rect);
        return intent;
    }

    class MyDrawable extends Drawable {
        private Paint paint;
        public MyDrawable(Bitmap bitmap) {
            paint = new Paint();
            BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            paint.setShader(shader);
        }

        @Override
        public void draw(Canvas canvas) {
            canvas.drawPaint(paint);
        }

        @Override
        public void setAlpha(int alpha) {
            paint.setAlpha(alpha);
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {
            paint.setColorFilter(colorFilter);
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSPARENT;
        }


    }
}
