package com.abedev.wahwbonus;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1001;
    private static final String KEY_URI = "uri";
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SAMPLES_FOLDER = "samples";
    private CropImageView imageView;
    private Uri uri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (CropImageView) findViewById(R.id.img);
        if (savedInstanceState != null) {
            uri = savedInstanceState.getParcelable(KEY_URI);
            loadImage();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = true;
        switch (item.getItemId()) {
            case R.id.selectImage:
                selectImage();
                break;
            case R.id.showTile:
                Rect rect = imageView.getCropRect();
                if (rect!=null)
                    startActivity(TileActivity.getIntent(this,uri,rect));
                break;
            case R.id.copyFromAssets:
                copyFromAssets();
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    private void copyFromAssets() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(MainActivity.this, "External storage not mounted!", Toast.LENGTH_SHORT).show();
            return;
        }
        String[] files = null;
        try {
            files = getAssets().list(SAMPLES_FOLDER);

        } catch (IOException e) {
            e.printStackTrace();
        }
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        for (String filename : files) {
            File outFile =  new File(file.getPath() + "/" + filename);
            if (outFile.exists())
                continue;
            InputStream inStream = null;
            OutputStream outStream = null;
            try {
                outFile.createNewFile();
                inStream = getAssets().open(SAMPLES_FOLDER+"/"+filename);
                outStream = new FileOutputStream(outFile);
                byte[] buffer = new byte[1024];
                int read;
                while ((read = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, read);
                }

            } catch (IOException ex) {
                Log.d(TAG,ex.getMessage());
                continue;
            } finally {
                if (inStream != null)
                    try {
                        inStream.close();
                    } catch (IOException e) {
                        Log.d(TAG,e.getMessage());
                    }
                if (outStream != null)
                    try {
                        outStream.close();
                    } catch (IOException e) {
                        Log.d(TAG,e.getMessage());
                    }
            }
            try {
                MediaStore.Images.Media.insertImage(getContentResolver(),outFile.getAbsolutePath(),filename,filename);
            } catch (FileNotFoundException e) {
                Log.d(TAG,e.getMessage());
            }
        }
    }

    private void selectImage() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    uri = data.getData();
                    loadImage();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadImage() {
        if (uri != null)
            imageView.setUri(uri);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_URI, uri);
    }

}
